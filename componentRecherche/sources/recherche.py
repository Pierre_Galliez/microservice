import flask
from flask import request, jsonify
from flask_cors import CORS
import mysql.connector

app = flask.Flask(__name__)
CORS(app)
app.config["DEBUG"] = True

@app.route('/', methods=['GET'])
def test():
	return "<h1>je être test</h1>"

@app.route('/recherche/ping', methods=['POST'])
def ping():
	return "up"

def connect():
	mybdd = mysql.connector.connect(host='bdd', port=3306, user='root',password='root', database='bdd')
	return mybdd

def requestSQL(recherche):
	mybdd = connect()
	mycursor = mybdd.cursor()
	mycursor.execute("SELECT DISTINCT nom, stock, description FROM produitt WHERE LOWER(nom) LIKE LOWER('%"+recherche+"%') OR LOWER(description) LIKE LOWER('%"+recherche+"%');")
	result = mycursor.fetchall()
	return result #une liste de tuple(nom,stock,description)

@app.route('/recherche/call', methods=['POST'])
def recherche():
	query_parameters = request.args
	critere = query_parameters.get('critere')
	result = requestSQL(critere) #pas de test sur le status faudra voir si les requete passent bien avec un indicateur
	if result is None:
		return jsonify(
		status="erreur recherche"
	)
	else:
		return jsonify(
			status="ok",
			listeRecherche=result 
		)

app.run(host="0.0.0.0")
