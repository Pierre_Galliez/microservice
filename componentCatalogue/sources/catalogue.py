import flask
from flask import request, jsonify
from flask_cors import CORS
import mysql.connector

app = flask.Flask(__name__)
CORS(app)
app.config["DEBUG"] = True

@app.route('/catalogue/call', methods=['POST'])
def chercher():
	return '''je suis le test'''

@app.route('/', methods=['GET'])
def test():
	return "<h1>je être test</h1>"

@app.route('/catalogue/ping', methods=['POST'])
def ping():
	return "up"

def connect():
	mybdd = mysql.connector.connect(host='bdd', port=3306, user='root',password='root', database='bdd')
	return mybdd

def requestSQL():
	mybdd = connect()
	mycursor = mybdd.cursor()
	mycursor.execute("SELECT nom, stock, description FROM produitt;")
	result = mycursor.fetchall()
	return result #une liste de tuple(nom,stock,description)

@app.route('/catalogue/loadAll', methods=['POST'])
def loadAll():
	tupleCatalogue = requestSQL()
	if(tupleCatalogue is None):
		return jsonify(status = "catalogue vide")
	else:
		return jsonify(
			status = "ok",
			listeCatalogue = tupleCatalogue #pas sur que ça focntionne parce que c'est pas du json le tuple		
		)

app.run(host="0.0.0.0")
