import flask
from flask import request, jsonify
from flask_cors import CORS
import mysql.connector

app = flask.Flask(__name__)
CORS(app)
app.config["DEBUG"] = True

@app.route('/panier/call', methods=['POST'])
def chercher():
	return '''je suis le test'''

@app.route('/', methods=['GET'])
def test():
	return "<h1>je être test</h1>"

@app.route('/panier/ping', methods=['POST'])
def ping():
	return "up"

def connect():
	mybdd = mysql.connector.connect(host='bdd', port=3306, user='root',password='root', database='bdd')
	return mybdd

def ajouterPanierSQL(produit, quantity, login):
	mybdd = connect()
	mycursor = mybdd.cursor()
	mycursor.execute("SELECT idProduit FROM produitt WHERE nom='"+produit+"';")
	idProduit = mycursor.fetchall()[0][0]
	mycursor.execute("SELECT idPanierEnCours, idClient FROM clientt WHERE nomCompte='"+login+"';")
	tupleRes = mycursor.fetchall()[0]
	idClient = tupleRes[1]
	idPanier = tupleRes[0]
	if idPanier == -1:
		mycursor.execute("SELECT MAX(idPanier) FROM paniert;")
		idPanier = mycursor.fetchall()[0][0] + 1
	#Verifier quon a pas deja un idpanier + idproduit, si c'est le cas bah quantite + quantity
	mycursor.execute("SELECT idProduit FROM paniert WHERE idProduit = "+str(idProduit)+" AND idPanier = "+str(idPanier)+";")
	if len(mycursor.fetchall()) != 0:
		mycursor.execute("UPDATE paniert set quantite=quantite+"+ str(quantity) +" where idProduit="+str(idProduit)+" AND idPanier = "+str(idPanier)+";")
		mybdd.commit()
		if mycursor.rowcount != 1:
			return False
	else:
		mycursor.execute("INSERT INTO paniert (idPanier, idClient, idProduit, quantite) VALUES (%s, %s, %s, %s);", (idPanier,idClient,idProduit,int(quantity)))
		mybdd.commit()
		if mycursor.rowcount != 1:
			return False
	mycursor.execute("UPDATE clientt SET idPanierEnCours = "+str(idPanier)+" WHERE nomCompte = '"+login+"';")
	mybdd.commit()
	if mycursor.rowcount != 1:
		return False
	return True  

@app.route('/panier/ajouterProduit', methods=['POST'])
def ajouterPanier():
	query_parameters = request.args
	produit = query_parameters.get('produit')
	quantity = query_parameters.get('quantity')
	client = query_parameters.get('client')
	if ajouterPanierSQL(produit, quantity, client):
		return jsonify(status="ok")
	else:
		return jsonify(status="notok")

def payerSQL(idPanier, login):
	mybdd = connect()
	mycursor = mybdd.cursor()
	mycursor.execute("SELECT idProduit, quantite FROM paniert where idPanier="+str(idPanier)+";")
	tuplePanier = mycursor.fetchall()
	for prod in tuplePanier:
		mycursor.execute("UPDATE produitt set stock=stock-"+ str(prod[1]) +" where idProduit="+str(prod[0])+";")
		mybdd.commit()
		if mycursor.rowcount != 1:
			return False
	tuplePanier = mycursor.fetchall()
	mycursor.execute("UPDATE clientt set idPanierEnCours=-1 where nomCompte = '" + login +"';")
	mybdd.commit()
	if mycursor.rowcount != 1:
		return False
	print("step 3")
	return True  

@app.route('/panier/payer', methods=['POST'])
def payer():
	query_parameters = request.args
	idPanier = query_parameters.get('idPanier')
	login = query_parameters.get('login')
	if payerSQL(idPanier, login):
		return jsonify(status="ok")
	else:
		return jsonify(status="notok")

def loadSQL(login):
	mybdd = connect()
	mycursor = mybdd.cursor()
	mycursor.execute("SELECT idClient, idPanierEnCours FROM clientt where nomCompte='"+login+"';")
	tupleRes = mycursor.fetchall()[0]
	idPanierEnCours = tupleRes[1]
	idClient = tupleRes[0]
	if idPanierEnCours != -1:
		mycursor.execute("SELECT nom, quantite FROM paniert, produitt WHERE paniert.idProduit = produitt.idProduit AND idPanier="+str(idPanierEnCours)+";")
		tuplePanier = mycursor.fetchall()
		return tuplePanier, idPanierEnCours
	return "vide", idPanierEnCours        

@app.route('/panier/load', methods=['POST'])
def load():
	query_parameters = request.args
	login = query_parameters.get('login')
	tuplePanier, idPanierEnCours = loadSQL(login)
	if idPanierEnCours >= 0:
		return jsonify(
			status = 'ok',
			listePanier = tuplePanier,
			idPanier = idPanierEnCours
		)
	elif idPanierEnCours == -1:
		return jsonify(
			status = 'ok', 
			listePanier = tuplePanier, 
			idPanier = idPanierEnCours
		)
	else:
		return jsonify(status="notok")

app.run(host="0.0.0.0")
